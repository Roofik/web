var quest = '';

var test = [{

		type: "radio",
		question: "С кем сегодня лучше побухать?",
		answers: [

			"С моим батей",
			"С твоей сестрой",
			"С моей собакой",
			"С моей ногой"

		],
		rightAnswer: 2

	}, {

		type: "radio",
		question: "Кто самый выдающийся ТЫЖПРОГРАМЕСТ в нашем университете?",
		answers: [

			"Богоявленский Ю.А.",
			"Димитров М.В.",
			"Уборщица",
			"Корзун Д.Ж."

		],
		rightAnswer: 1

	}, {

		type: "checkbox",
		question: "Какие самые главные предметы в текущем курсе?",
		answers: [

			"Языки программирования и методы трансляции",
			"Деловые коммуникации",
			"Теория вероятности",
			"Информационные системы в бизнесе",
			"Технологии производства программного обеспечения",
			"Web-технологии"

		],
		rightAnswer: [0, 1, 3, 4]

	}, {

		type: "text",
		question: "А теперь вопрос послежнее. Вставь слово, которое больше подходим по смыслу. \"ПетрГУ - __________ ВУЗ Карелии\"",
		rightAnswer: "опорный"

	}];

function LoadQuestions()
{
	for (var i = 0; i < test.length; i++)
	{
		if (test[i].type == "radio" || test[i].type == "checkbox")
		{
			LoadRadioOrCheckboxQuestion(test[i], i);
		}

		if (test[i].type == "text")
		{
			LoadTextQuestion(test[i], i);
		}
	}

	document.getElementById('question').innerHTML = quest;
}

function LoadRadioOrCheckboxQuestion(question, num)
{
	quest += '<div id="' + 'question' + num + '">';
	quest += '<p>' + question.question + '</p>';
	quest += '<ul>';
	for (var i = 0; i < question.answers.length; i++)
	{
		quest += '<li><input type="' + question.type + '" name="answer' + num + '" id="question' + num + 'answer' + i + '"><label for="question' + num + 'answer' + i + '">' + question.answers[i] + '</label></li>';
	}
	quest += '</ul>';
	quest += '</div>';
}

function LoadTextQuestion(question, num)
{
	quest += '<div id="' + 'question' + num + '">';
	quest += '<p>' + question.question + '</p>';
	quest += '<ul>';
	quest += '<li><input type="' + question.type + '" name="answer' + num + '" id="question' + num + 'answer0"></li>';
	quest += '</ul>';
	quest += '</div>';
}

function LoadResult()
{
	document.getElementById('gray').style.display = 'block';
	document.getElementById('result').style.display = 'block';

	quest = '';
	quest += '<form action="">';
	quest += '<input type="submit" id="close" value="close">';
	quest += '</form>';
	quest += '<h2>Ваш результат</h2>';

	for (var i = 0; i < test.length; i++)
	{
		answers = document.getElementsByName('answer' + i);
		quest += '<p>Вопрос ' + (i + 1) + ': '
		if (test[i].type == 'radio')
		{
			CheckAnswerRadio(test[i], answers);
		}

		if (test[i].type == 'checkbox')
		{
			CheckAnswerCheckbox(test[i], answers);
		}
		if (test[i].type == 'text')
		{
			CheckAnswerText(test[i], answers);
		}
	}

	document.getElementById('result').innerHTML = quest;
}

function CheckAnswerRadio(question, answers)
{
	for (var j = 0; j < answers.length; j++)
	{
		if (answers[j].checked)
		{
			if (j == question.rightAnswer)
			{
				quest += '<span>верно</span></p>';
				break;
			}
		}
		else
		{
			if (j == answers.length - 1)
			{
				quest += '<span>не верно</span></p>';
			}
		}
	}
}

function CheckAnswerCheckbox(question, answers)
{
	var chooseAnswer = [];

	for (var i = 0; i < answers.length; i++)
	{
		if (answers[i].checked)
		{
			chooseAnswer.push(i);
		}
	}

	if (chooseAnswer.length == question.rightAnswer.length)
	{
		for (var i = 0; i < chooseAnswer.length; i++)
		{
			if (chooseAnswer[i] != question.rightAnswer[i])
			{
				quest += '<span>не верно</span></p>';
				break;
			}

			if (i == chooseAnswer.length - 1)
			{
				quest += '<span>верно</span></p>';
			}
		}
	}
	else
	{
		quest += '<span>не верно</span></p>';
	}
}

function CheckAnswerText(question, answer)
{
	if (question.rightAnswer == answer[0].value)
	{
		quest += '<span>верно</span></p>';
	} 
	else
	{
		quest += '<span>не верно</span></p>';
	}
	else
	{
		quest += '<span>не верно</span></p>';
	}
}