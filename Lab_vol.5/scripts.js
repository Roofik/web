function Init()
{
	RangeChanging("size", "value");

	LoadDate();
}

function RangeChanging(id, value)
{
	size = document.getElementById(id);
	span = document.getElementById(value);

	span.innerHTML = "value = " + size.value;
}

function LoadDate()
{
	var nowDate = new Date(Date.now());
	var date = document.getElementById("date");

	date.value = nowDate.getFullYear() + "-" + nowDate.getMonth() + "-" + nowDate.getDate();
}